import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by avi on 15/1/16.
 */
public class WebHandle
{
	private static String google(String query) throws IOException
	{
		String address = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=";
		String charset = "UTF-8";

		URL url = new URL(address + URLEncoder.encode(query, charset));

		BufferedReader in = new BufferedReader(new InputStreamReader(
				url.openStream()));

		String searchResult = "";

		while ((searchResult = in.readLine()) != null) {
			System.out.println(searchResult);
		}

		in.close();

		return searchResult;
	}

	public static void main(String[] args)
	{
		final String folder = "/media/avi/Glasgow's Riches/Videos/Movies";

		List<String> movies = FileHandle.getMovies(folder);
		final int size  = movies.size();

		int index = 1;
		for(String movieName : movies)
		{
			System.out.println(movieName + "(" + index++ + "/" + size + ")");
			try
			{
				google(movieName + " imdb");
			} catch (IOException ioException)
			{
				System.out.println("Exception");
			}
		}
	}
}
