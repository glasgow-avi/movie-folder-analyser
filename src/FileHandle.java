import java.io.File;
import java.util.ArrayList;

/**
 * Created by avi on 14/1/16.
 */
public class FileHandle
{
	private static ArrayList<String> moviePaths = new ArrayList<String>(), movies = new ArrayList<String>();

	public static ArrayList getMovies(String folder) //returns the names of all of the movie files in the folder
	{
		movies.clear();
		moviePaths.clear();
		recurseListMovies(folder);
		return movies;
	}

	public static ArrayList getMoviePaths(String folder)
	{
		movies.clear();
		moviePaths.clear();
		recurseListMovies(folder);
		return moviePaths;
	}

	public static void recurseListMovies(String folder) //recursively lists a folder and retrieves all the movies into the array lists
	{
		File f = new File(folder);

		String[] content = f.list(); //everything in that current folder

		for(String file : content) //iterating through the contents of the folder
		{
			String newPath = folder + "/" + file; //path of the current iteration
			File newFile = new File(newPath); //file corresponding to the current iteration

			boolean isFileAVideo = file.contains(".vlc") ||
					file.contains(".mkv") ||
					file.contains(".wav") ||
					file.contains(".mp4") ||
					file.contains(".wmv") ||
					file.contains(".avi") ||
					file.contains(".mov");
			//check if the file name contains any of the file extensions

			if(isFileAVideo)
			{
				movies.add(file); //add the file name to the movies list
				moviePaths.add(newPath); //add the file path to the movie paths list
			}

			if(newFile.isDirectory()) //if the current iteration file is a directory
				recurseListMovies(newPath); //recurse for that directory
		}
	}



	private static void recurseList(String folder, String indentation) //ls -R
	{
		File f = new File(folder);

		String[] content = f.list(); //everything in that current folder

		for(String file : content) //iterating through the contents of the folder
		{
			String newPath = folder + "/" + file; //path of the current iteration
			File newFile = new File(newPath);

			System.out.println(indentation + file);
			if(file.contains(".dat") || file.contains(".jpg") || file.contains(".txt") || file.contains(".db")) //delete all junk files that came bundled with the torrent
				newFile.delete();

			if(newFile.isDirectory())
				recurseList(newPath, "    " + indentation);
		}
	}

	public static void recurseList(String folder) //ls -R folder
	{
		recurseList(folder, "");
	}

	public static void main(String[] args)
	{
		final String folder = "/media/avi/Glasgow's Riches/Videos/Movies";

		recurseList(folder);
		getMovies(folder);

		for(String videoFile : movies)
			System.out.println(videoFile);
	}
}